@extends('layouts.app')

@section('titulo')
  <title>Switches Editar</title>
@endsection
@section('content')
  <style>
   	.card-header{
   		background-color: #3490dc;
   		color: #ffffff;
   	}
  </style>

	<div class="container">
		<div class="card-header">
			Editar Switch
		</div>

		<div class="card-body">

  		<form action="{{route('switches.update', $switch->id)}}" method="post">
  			@method('PATCH')
  			@csrf
  		<div class="form-group">
				<label for="marca">Marca</label>
                    <div class="col">
			<input type="text" class="form-control @error ('Marca') is-invalid  @enderror" name="Marca" value="{{$switch->Marca}}">
            @error('Marca')
            <span class="invalid-feedback" role-"alert">
            <strong>{{ $message}}</strong>
            </span>
            @enderror
                    </div>
                    </div>

		<div class="form-group">
				<label for="caracteristicas">Caracteristicas</label>
                    <div class="col">
			<input type="text" class="form-control @error ('Caracteristicas') is-invalid  @enderror" name="Caracteristicas" value="{{$switch->Caracteristicas}}">
            @error('Caracteristicas')
            <span class="invalid-feedback" role-"alert">
            <strong>{{ $message}}</strong>
            </span>
            @enderror
                    </div>
                    </div>

        <div class="form-group">
				<label for="ip_admin">IP Administrativa</label>
                    <div class="col">
			<input type="text" class="form-control @error ('ip_admin') is-invalid  @enderror" name="ip_admin" value="{{$switch->ip_admin}}">
            @error('ip_admin')
            <span class="invalid-feedback" role-"alert">
            <strong>{{ $message}}</strong>
            </span>
            @enderror
                    </div>
                    </div>

  			<button type="submit" class="btn btn-primary">Actualizar</button>
  			<a href="{{ url()->previous() }}" class="btn btn-danger">Cancelar</a>
  		</form>
		</div>
	</div>
@endsection
