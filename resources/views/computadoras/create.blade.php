@extends('layouts.app')

@section('content')
<style>
	.card-header{
		background-color: #4390dc;
		color: #ffffff;
	}
</style>

<div class="container">
	<div class="card-header">
		Agregar Computadora
	</div>
	<div class="card-body">

		<form action="{{route('computadoras.store')}}" method="post">
			@csrf

			<div class="form-group">
				<label for="Nombre">Nombre</label>
				<div class="col">
					<input type="text" class="form-control @error('Nombre') is-invalid @enderror" name="Nombre" value="{{old('Nombre')}}">

          @error('Nombre')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
          @enderror
      	</div>
			</div>

			<div class="form-group">
				<label for="departamento">Departamento</label>
				<div class="col">
					<select  id="Departamento" name="Departamento" class="form-control @error('Departamento') is-invalid @enderror">
		                       <option value="" disabled selected>Selecciona un Departamento</option>
		                       @foreach($departamentos as $departamento)
		                       	<option value="{{$departamento->id}}">{{$departamento->nombre}}  </option>
		                       @endforeach
		     	</select>

					@error('Departamento')
							<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
							</span>
					@enderror
				</div>
			</div>

			<div class="form-group">
				<label for="SO">Sistema Operativo</label>

				<div class="col">
					<input type="text" class="form-control @error('SO') is-invalid @enderror" name="SO" value="{{old('SO')}}">

					@error('SO')
							<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
							</span>
					@enderror
				</div>
			</div>

			<div class="form-group">
				<label for="ram">Memoria Ram</label>
				<div class="col">
					<input type="text" class="form-control @error('Ram') is-invalid @enderror" name="Ram" value="{{old('Ram')}}">

					@error('Ram')
							<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
							</span>
					@enderror
				</div>
			</div>

			<div class="form-group">
				<label for="disco">Disco Duro</label>
				<div class="col">
					<input type="text" class="form-control @error('Disco') is-invalid @enderror" name="Disco" value="{{old('Disco')}}">

					@error('Disco')
							<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
							</span>
					@enderror
				</div>
			</div>

			<div class="form-group">
				<label for="procesador">Procesador</label>
				<div class="col">
					<input type="text" class="form-control @error('Procesador') is-invalid @enderror" name="Procesador" value="{{old('Procesador')}}">

					@error('Procesador')
							<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
							</span>
					@enderror
				</div>
			</div>

			<div class="form-group">
				<label for="modelo">Modelo</label>
				<div class="col">
					<input type="text" class="form-control @error('Modelo') is-invalid @enderror" name="Modelo" value="{{old('Modelo')}}">

					@error('Modelo')
							<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
							</span>
					@enderror
				</div>
			</div>

			<div class="form-group">
				<label for="caracteristicas">Caracteristicas</label>
				<div class="col">
					<input type="text" class="form-control @error('Caracteristicas') is-invalid @enderror" name="Caracteristicas" value="{{old('Caracteristicas')}}">

					@error('Caracteristicas')
							<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
							</span>
					@enderror
				</div>
			</div>

			<div class="form-group">
				<label for="encargado">Encargado</label>
				<div class="col">
					<input type="text" class="form-control @error('encargado') is-invalid @enderror" name="encargado" value="{{old('encargado')}}">

					@error('encargado')
							<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
							</span>
					@enderror
				</div>
			</div>

			<div class="form-group">
				<label for="ip">Dirección IP</label>
				<div class="col">
					<input type="text" class="form-control @error('ip') is-invalid @enderror" name="ip" value="{{old('ip')}}">

					@error('ip')
							<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
							</span>
					@enderror
				</div>
			</div>

			<button type="submit" class="btn btn-primary">Guardar</button>
			<a href="{{ url()->previous() }}" class="btn btn-danger">Cancelar</a>
		</form>
	</div>
</div>
@endsection
