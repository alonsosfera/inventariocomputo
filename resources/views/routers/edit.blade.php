@extends('layouts.app')

@section('titulo')
  <title>Routers Editar</title>
@endsection
@section('content')
  <style>
   	.card-header{
   		background-color: #3490dc;
   		color: #ffffff;
   	}
  </style>

	<div class="container">
		<div class="card-header">
			Editar Router
		</div>

		<div class="card-body">

  		<form action="{{route('routers.update', $router->id)}}" method="post">
  			@method('PATCH')
  			@csrf

  		 <div class="form-group">
				<label for="marca">Marca</label>
                    <div class="col">
			<input type="text" class="form-control @error ('Marca') is-invalid  @enderror" name="Marca" value="{{$router->Marca}}">
            @error('Marca')
            <span class="invalid-feedback" role-"alert">
            <strong>{{ $message}}</strong>
            </span>
            @enderror
                    </div>
                    </div>

			<div class="form-group">
				<label for="caracteristicas">Caracteristicas</label>
                    <div class="col">
			<input type="text" class="form-control @error ('Caracteristicas') is-invalid  @enderror" name="Caracteristicas" value="{{$router->Caracteristicas}}">
            @error('Caracteristicas')
            <span class="invalid-feedback" role-"alert">
            <strong>{{ $message}}</strong>
            </span>
            @enderror
                    </div>
                    </div>

            <div class="form-group">
				<label for="ip_wan">IP WAN</label>
                    <div class="col">
			<input type="text" class="form-control @error ('ip_wan') is-invalid  @enderror" name="ip_wan" value="{{$router->ip_wan}}">
            @error('ip_wan')
            <span class="invalid-feedback" role-"alert">
            <strong>{{ $message}}</strong>
            </span>
            @enderror
                    </div>
                    </div>

            <div class="form-group">
				<label for="ip_lan">IP LAN</label>
                    <div class="col">
			<input type="text" class="form-control @error ('ip_lan') is-invalid  @enderror" name="ip_lan" value="{{$router->ip_lan}}">
            @error('ip_lan')
            <span class="invalid-feedback" role-"alert">
            <strong>{{ $message}}</strong>
            </span>
            @enderror
                    </div>
                    </div>

  			<button type="submit" class="btn btn-primary">Actualizar</button>
  			<a href="{{ url()->previous() }}" class="btn btn-danger">Cancelar</a>
  		</form>
		</div>
	</div>
@endsection
