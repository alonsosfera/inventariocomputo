@extends('layouts.app')
@section('content')
<style>
	.card-header{
		background-color: #4390dc;
		color: #ffffff;
	}
</style>

<div class="container">
	<div class="card-header">
		Agregar Router
	</div>
	<div class="card-body">
		<form action="{{route('routers.store')}}" method="post">
			@csrf

			 <div class="form-group">
				<label for="marca">Marca</label>
        <div class="col">
					<input type="text" class="form-control @error ('Marca') is-invalid  @enderror" name="Marca" value="{{old('Marca')}}">
          @error('Marca')
	          <span class="invalid-feedback" role-"alert">
	          	<strong>{{ $message}}</strong>
	          </span>
          @enderror
        </div>
      </div>

			<div class="form-group">
				<label for="caracteristicas">Caracteristicas</label>
                    <div class="col">
			<input type="text" class="form-control @error ('Caracteristicas') is-invalid  @enderror" name="Caracteristicas" value="{{old('Caracteristicas')}}">
            @error('Caracteristicas')
            <span class="invalid-feedback" role-"alert">
            <strong>{{ $message}}</strong>
            </span>
            @enderror
                    </div>
                    </div>

            <div class="form-group">
				<label for="ip_wan">IP WAN</label>
                    <div class="col">
			<input type="text" class="form-control @error ('ip_wan') is-invalid  @enderror" name="ip_wan" value="{{old('ip_wan')}}">
            @error('ip_wan')
            <span class="invalid-feedback" role-"alert">
            <strong>{{ $message}}</strong>
            </span>
            @enderror
                    </div>
                    </div>

            <div class="form-group">
				<label for="ip_lan">IP LAN</label>
                    <div class="col">
			<input type="text" class="form-control @error ('ip_lan') is-invalid  @enderror" name="ip_lan" value="{{old('ip_lan')}}">
            @error('ip_lan')
            <span class="invalid-feedback" role-"alert">
            <strong>{{ $message}}</strong>
            </span>
            @enderror
                    </div>
                    </div>

			<button type="submit" class="btn btn-primary">Guardar</button>
			<a href="{{ url()->previous() }}" class="btn btn-danger">Cancelar</a>
		</form>
	</div>
</div>




@endsection
