@extends('layouts.app')

@section('content')
<style>
	.card-header{
		background-color: #4390dc;
		color: #ffffff;
	}
</style>

<div class="container">
  <div class="col-sm-6 mx-auto">
  	<div class="card-header">
  		Registrar Usuario
  	</div>
  	<div class="card-body">
  		<form action="{{ route('register') }}" method="post">
  			@csrf

  			<div class="form-group">
  				<label for="username">Nombre de Usuario</label>
          <div class="col">
  					<input type="text" class="form-control @error ('username') is-invalid  @enderror" name="username" value="{{old('username')}}" required autocomplete="username" autofocus>
            @error('username')
  	          <span class="invalid-feedback" role-"alert">
  	          	<strong>{{ $message}}</strong>
  	          </span>
            @enderror
          </div>
        </div>

  			<div class="form-group">
  				<label for="cargo">Tipo de Cargo</label>
          <div class="col">
            <select class="form-control @error ('cargo') is-invalid  @enderror" name="cargo" value="{{old('cargo')}}" required>
              <option value="Mantenimiento" selected>Mantenimiento</option>
              <option value="Administrador"> Administrador</option>
            </select>
            @error('cargo')
  	          <span class="invalid-feedback" role-"alert">
  	          	<strong>{{ $message}}</strong>
  	          </span>
            @enderror
          </div>
        </div>

  			<button type="submit" class="btn btn-primary">Registrar</button>
  			<a href="{{ url()->previous() }}" class="btn btn-danger">Cancelar</a>
  		</form>
  	</div>

  </div>
</div>




@endsection
