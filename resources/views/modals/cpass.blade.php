<!-- Change pswd Modal -->
    <div class="modal fade bd-example-modal-lg" id="modalPass" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="TituloModal">Cambiar contraseña </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
            <div class="modal-body">
              <form method="POST" action="/reusuarios/">
                @csrf
                <div class="form-group">
          				<label for="password">{{ __('Password') }}</label>
                  <div class="col">
          					<input type="password" class="form-control @error ('password') is-invalid  @enderror" name="password" required autocomplete="new-password">
                    @error('password')
          	          <span class="invalid-feedback" role-"alert">
          	          	<strong>{{ $message}}</strong>
          	          </span>
                    @enderror
                  </div>
                </div>

                <div class="form-group">
          				<label for="password-confirm">{{ __('Confirm Password') }}</label>
                  <div class="col">
          					<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                  </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-6">
                        <button type="submit" class="btn btn-primary">
                            {{ __('Reset Password') }}
                        </button>
                    </div>
                </div>
                </form>
        </div>
      </div>
    </div>
  </div>

    <!-- /Password Modal -->
