@extends('layouts.app')

@section('titulo')
  <title>Impresoras Editar</title>
@endsection
@section('content')
  <style>
   	.card-header{
   		background-color: #3490dc;
   		color: #ffffff;
   	}
  </style>

	<div class="container">
		<div class="card-header">
			Editar Impresora
		</div>

		<div class="card-body">

  		<form action="{{route('impresoras.update', $impresora->id)}}" method="post">
  			@method('PATCH')
  			@csrf
        <div class="form-group">
  				<label for="marca">Marca</label>
          <div class="col">
  					<input type="text" class="form-control @error ('Marca') is-invalid  @enderror" name="Marca" value="{{$impresora->Marca}}">
            @error('Marca')
  	          <span class="invalid-feedback" role-"alert">
  	          	<strong>{{ $message}}</strong>
  	          </span>
            @enderror
          </div>
        </div>

  			<div class="form-group">
  				<label for="departamento">Departamento</label>
  				<div class="col">
  					<select  id="Departamento" name="Departamento" class="form-control @error('Departamento') is-invalid @enderror">
  		                       <option value="{{$impresora->Departamento}}" disabled selected>{{$departamentoImpresoras->nombre}}</option>
  		                       @foreach($departamentos as $departamento)
  		                       	<option value="{{$departamento->id}}">{{$departamento->nombre}}  </option>
  		                       @endforeach
  		     	</select>

  					@error('Departamento')
  							<span class="invalid-feedback" role="alert">
  									<strong>{{ $message }}</strong>
  							</span>
  					@enderror
  				</div>
  			</div>


  	     <div class="form-group">
  				<label for="caracteristicas">Caracteristicas</label>
          <div class="col">
  					<input type="text" class="form-control @error ('Caracteristicas') is-invalid  @enderror" name="Caracteristicas" value="{{$impresora->Caracteristicas}}">
              @error('Caracteristicas')
  	            <span class="invalid-feedback" role-"alert">
  	            	<strong>{{ $message}}</strong>
  	            </span>
              @enderror
            </div>
          </div>

  			<button type="submit" class="btn btn-primary">Actualizar</button>
  			<a href="{{ url()->previous() }}" class="btn btn-danger">Cancelar</a>
  		</form>
		</div>
	</div>
@endsection
