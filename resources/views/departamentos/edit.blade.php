@extends('layouts.app')

@section('titulo')
  <title>Departamentos Editar</title>
@endsection
@section('content')
  <style>
   	.card-header{
   		background-color: #3490dc;
   		color: #ffffff;
   	}
  </style>

	<div class="container">
		<div class="card-header">
			Editar Departamento
		</div>

		<div class="card-body">
			@if($errors->any())
		    <div class="alert alert-danger">
    			<ul>
    				@foreach($errors->all() as $error)
    					<li> {{$error}} </li>
    				@endforeach
    			</ul>
	      </div><br>
      @endif

  		<form action="{{route('departamentos.update', $departamento->id)}}" method="post">
  			@method('PATCH')
  			@csrf

  			<div class="form-group">
  			<label form="nombre">Nombre</label>
  			<input type="text" class="form-control" name="nombre" value="{{$departamento->nombre}}">
  			</div>



  			<button type="submit" class="btn btn-primary">Actualizar</button>
  			<a href="{{ url()->previous() }}" class="btn btn-danger">Cancelar</a>
  		</form>
		</div>
	</div>
@endsection
