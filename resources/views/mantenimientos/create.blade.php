@extends('layouts.app')

@section('content')
<style>
	.card-header{
		background-color: #4390dc;
		color: #ffffff;
	}
</style>

<div class="container">
	<div class="card-header">
		Registrar Mantenimiento
	</div>
	<div class="card-body">
		@if($errors->any())
			<div class="alert alert-danger">
				<ul>
					@foreach($errors->all() as $error)
						<li> {{$error}} </li>
					@endforeach
				</ul>
			</div><br>
		@endif

		<form action="{{route('mantenimientos.store')}}" method="post">
			@csrf


			<div class="form-group">
				<label for="equipo">Equipo</label>
				<button type="button" id="equipobtn" class="btn btn-secondary btn-block" data-toggle="modal" data-target="#InventarioModal">Seleccionar Equipo</button>
			</div>

			<div class="form-group">
				<label for="Nombre">Nombre</label>
			<input type="text" class="form-control" name="Nombre" value="{{old('Nombre')}}"></div>

			<div class="form-group">
				<p>Which programming languages do you like?</p>
         <form>
            <label class = "checkbox-inline">
               <input type = "checkbox" value = "">Java
            </label>
            <label class = "checkbox-inline">
               <input type="checkbox" value="">C++
            </label>
            <label class = "checkbox-inline">
               <input type = "checkbox" value = "">C
            </label>
         </form>
			</div>

			<button type="submit" class="btn btn-primary">Agregar</button>
			<a href="{{ url()->previous() }}" class="btn btn-danger">Cancelar</a>
		</form>
	</div><br><hr/>

	<h4>Orden de Mantenimiento</h4>

	<div class="table-responsive">
		<table class="table table-stripped">
			<thead>
				<tr>
					<td align="left" colspan="100%" style="padding:5px"><a href="{{route('mantenimientos.create')}}" class="btn btn-primary">Nuevo</a></td>
				</tr>
				<tr>
					<td>ID</td>
					<td>ID Equipo</td>
					<td>Equipo</td>
					<td>Encargado</td>
					<td>Mantenimiento</td>
					<td>Observaciones</td>
					<td colspan="100%">Acciones</td>
				</tr>

			</thead>
			<tbody>
				@foreach($mantenimientos as $mantenimiento)
					<tr>
						<td>{{$mantenimiento->id}}</td>
						<td> <a href="#"> {{$mantenimiento->id_Equipo}}</a></td>
						<td>{{$mantenimiento->tipo_equipo}}</td>
						<td>{{$mantenimiento->usuario}}</td>
						<td>{{$mantenimiento->acciones}}</td>
						<td>{{$mantenimiento->observaciones}}</td>
						<td align="center"><a href="{{route('mantenimientos.edit', $mantenimiento->id)}}" class="btn btn-primary" title="Editar"><i class="fa fa-edit" aria-hidden="true"></i></a></td>
						<td align="center">
						<form action="{{route('mantenimientos.destroy', $mantenimiento->id)}}" method="post">
							@csrf
							@method('DELETE')
							<button type="submit" class="btn btn-danger" onclick="return confirm('¿Estas seguro de eliminar?')" title="Borrar"><i class="fa fa-trash" aria-hidden="true"></i></button>
						</form>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>

	</div>
</div>
@endsection
