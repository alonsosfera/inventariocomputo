@extends('layouts.app')

@section('titulo')
  <title>Computadoras Editar</title>
@endsection
@section('content')
  <style>
   	.card-header{
   		background-color: #3490dc;
   		color: #ffffff;
   	}
  </style>

	<div class="container">
		<div class="card-header">
			Editar Computadora
		</div>

		<div class="card-body">
			@if($errors->any())
		    <div class="alert alert-danger">
    			<ul>
    				@foreach($errors->all() as $error)
    					<li> {{$error}} </li>
    				@endforeach
    			</ul>
	      </div><br>
      @endif

  		<form action="{{route('computadoras.update', $computadora->id)}}" method="post">
  			@method('PATCH')
  			@csrf
  			<div class="form-group">
  			<label form="SO">Sistema Operativo</label>
  			<input type="text" class="form-control" name="SO" value="{{$computadora->SO}}">
  			</div>

  			<div class="form-group">
  			<label form="Ram">Memoria Ram</label>
  			<input type="text" class="form-control" name="Ram" value="{{$computadora->Ram}}">
  			</div>

  			<div class="form-group">
  			<label form="Disco">Disco Duro</label>
  			<input type="text" class="form-control" name="Disco" value="{{$computadora->Disco}}">
  			</div>

  			<div class="form-group">
  			<label form="Procesador">Procesador</label>
  			<input type="text" class="form-control" name="Procesador" value="{{$computadora->Procesador}}">
  			</div>

  			<div class="form-group">
  			<label form="Modelo">Modelo</label>
  			<input type="text" class="form-control" name="Modelo" value="{{$computadora->Modelo}}">
  			</div>

  			<div class="form-group">
  			<label form="Departamento">Departamento</label>
  			<select  id="Departamento" name="Departamento" class="form-control">

                         <option value="{{$computadora->Departamento}}">{{$departamento->nombre}}</option>


                         @foreach($departamentos as $departamento)
                          <option value="{{$departamento->id}}">  {{$departamento->nombre}}  </option>
                         @endforeach
                         </select>
  			</div>

  			<div class="form-group">
  			<label form="Caracteristicas">Caracteristicas</label>
  			<input type="text" class="form-control" name="Caracteristicas" value="{{$computadora->Caracteristicas}}">
  			</div>

  			<button type="submit" class="btn btn-primary">Actualizar</button>
  			<a href="{{ url()->previous() }}" class="btn btn-danger">Cancelar</a>
  		</form>
		</div>
	</div>
@endsection
