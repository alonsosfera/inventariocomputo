@extends('layouts.app')

@section('titulo')
  <title>Computadoras</title>
@endsection
@section('content')
  <div class="container">
  	<h1>Computadoras</h1>

  	<hr/>

  	@if(session()->get('mensaje'))
  	<div class="alert alert-succes">

  		{{session()->get('mensaje')}}
  	</div><br>

  	@endif
    <div class="table-responsive">
    	<table class="table table-stripped">
    		<thead>
          <tr>
            <td align="left" colspan="100%" style="padding:5px"><a href="{{route('computadoras.create')}}" class="btn btn-primary">Nuevo</a></td>
          </tr>
    			<tr>
    				<td>ID</td>
    				<td>Departamento</td>  
            <td>Sistema Operativo</td>
    				<td>Ram</td>
    				<td>Disco</td>
    				<td>Procesador</td>
    				<td>Modelo</td>
    				
    				<td>Caracteristicas</td>
    				
    			</tr>

    		</thead>
    		<tbody>
    			@foreach($computadoras as $computadora)
    				<tr>
    					<td>{{$computadora->id}}</td>
    					 <td>{{$deps[$computadora->Departamento]}}</td>
              <td>{{$computadora->SO}}</td>
    					<td>{{$computadora->Ram}}</td>
    					<td>{{$computadora->Disco}}</td>
    					<td>{{$computadora->Procesador}}</td>
    					<td>{{$computadora->Modelo}}</td>
    					
    					<td>{{$computadora->Caracteristicas}}</td>
    					<td align="center"><a href="{{route('computadoras.edit', $computadora->id)}}" class="btn btn-primary" title="Editar"><i class="fa fa-edit" aria-hidden="true"></i></a></td>
    					<td align="center">
  						<form action="{{route('computadoras.destroy', $computadora->id)}}" method="post">
  							@csrf
  							@method('DELETE')
  							<button type="submit" class="btn btn-danger" onclick="return confirm('¿Estas seguro de eliminar?')" title="Borrar"><i class="fa fa-trash" aria-hidden="true"></i></button>
  						</form>
    					</td>
    				</tr>
    			@endforeach
    		</tbody>
  		</table>
    </div>
  </div>
@endsection
