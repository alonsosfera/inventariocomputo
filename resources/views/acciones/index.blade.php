@extends('layouts.app')

@section('titulo')
  <title>Acciones</title>
@endsection
@section('content')
  <div class="container">
  	<h1>Acciones</h1>

  	<hr/>

  	@if(session()->get('mensaje'))
  	<div class="alert alert-succes">

  		{{session()->get('mensaje')}}
  	</div><br>

  	@endif
    <div class="table-responsive">
    	<table class="table table-stripped">
    		<thead>
          <tr>
            <td align="left" colspan="100%" style="padding:5px"><a href="{{route('acciones.create')}}" class="btn btn-primary">Nuevo</a></td>
          </tr>
    			<tr>
    				<td>ID</td>
    				<td>Nombre</td>
    				<td colspan="100%">Acciones</td>
    			</tr>

    		</thead>
    		<tbody>
    			@foreach($acciones as $accion)
    				<tr>
    					<td>{{$accion->id}}</td>
    					<td>{{$accion->nombre}}</td>
              <td><a href="{{route('acciones.show',$accion->id)}}" class="btn btn-primary">Ver</a></td>
    					<td><a href="{{route('acciones.edit', $accion->id)}}" class="btn btn-primary">Editar</a></td>
    					<td>
  						<form action="{{route('acciones.destroy', $accion->id)}}" method="post">
  							@csrf
  							@method('DELETE')
  							<button type="submit" class="btn btn-danger" onclick="return confirm('¿Estas seguro de eliminar?')" >Borrar</button>
  						</form>
    					</td>
    				</tr>
    			@endforeach
    		</tbody>
  		</table>
    </div>
  </div>
@endsection