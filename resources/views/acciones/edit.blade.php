@extends('layouts.app')

@section('titulo')
  <title>Departamentos Editar</title>
@endsection
@section('content')
  <style>
   	.card-header{
   		background-color: #3490dc;
   		color: #ffffff;
   	}
  </style>

	<div class="container">
		<div class="card-header">
			Editar Accion
		</div>

		<div class="card-body">
			@if($errors->any())
		    <div class="alert alert-danger">
    			<ul>
    				@foreach($errors->all() as $error)
    					<li> {{$error}} </li>
    				@endforeach
    			</ul>
	      </div><br>
      @endif

  		<form action="{{route('acciones.update', $accion->id)}}" method="post">
  			@method('PATCH')
  			@csrf
  			<div class="form-group">
  			<label form="id">Id</label>
  			<input type="text" class="form-control" name="id" value="{{$accion->id}}">
  			</div>

  			<div class="form-group">
  			<label form="nombre">Nombre</label>
  			<input type="text" class="form-control" name="nombre" value="{{$accion->nombre}}">
  			</div>

  	

  			<button type="submit" class="btn btn-primary">Actualizar</button>
  		</form>
		</div>
	</div>
@endsection