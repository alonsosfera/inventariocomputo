@extends('layouts.app')
@section('content')
<style>
	.card-header{
		background-color: #4390dc;
		color: #ffffff;
	}
</style>

<div class="container">
	<div class="card-header">
		Agregar Accion
	</div>
	<div class="card-body">
		@if($errors->any())
			<div class="alert alert-danger">
				<ul>
					@foreach($errors->all() as $error)
						<li> {{$error}} </li>
					@endforeach
				</ul>
			</div><br>
		@endif
		<form action="{{route('acciones.store')}}" method="post">
			@csrf
			

			<div class="form-group">
				<label for="nombre">Nombre</label>
			<input type="text" class="form-control" name="nombre" value="{{old('nombre')}}"></div>

			
			<button type="submit" class="btn btn-primary">Guardar</button>
			<a href="{{route('departamentos.index')}}" class="btn btn-danger">Cancelar</a>
		</form>
	</div>
</div>




@endsection