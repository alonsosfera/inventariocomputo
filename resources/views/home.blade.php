@extends('layouts.app')

@section('content')
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row3">
  <main class="hoc container clear">
    <!-- main body -->
    <!-- ################################################################################################ -->
    <div class="group">
      <div class="one_third first">
        <!-- ################################################################################################ -->
        <h3 class="heading btmspace-50">Inventario</h3>
        <article class="element btmspace-30"><i class="fas fa-inbox"></i>
          <h3 class="heading"><a href="{{route('routers.index')}}">Routers</a></h3>
        </article>
        <article class="element btmspace-30"><i class="fas fa-hdd-o"></i>
          <h3 class="heading"><a href="{{route('switches.index')}}">Switches</a></h3>
        </article>
        <!--<article class="element btmspace-30"><i class="fa fa-suitcase"></i>
          <h3 class="heading"><a href="#">Leo bibendum eget</a></h3>
          <p>Magna metus pretium sed porttitor non laoreet sed quam vestibulum ante ipsum&hellip;</p>
        </article>
        <article class="element"><i class="fa fa-video-camera"></i>
          <h3 class="heading"><a href="#">Velit in aliquet</a></h3>
          <p>Ultrices posuere cubilia curae nulla sodales laoreet mollis cras posuere et risus vehicula&hellip;</p>
        </article>-->
        <!-- ################################################################################################ -->
      </div>
      <div class="two_third blocks">
        <div class="group">
          <!-- ################################################################################################ -->
          <article class="one_half first btmspace-20"><a href="{{route('mantenimientos.index')}}"><i class="fas fa-hammer"></i>
            <h3 class="heading">Mantenimiento</h3>
            </a></article>
          <article class="one_half btmspace-20"><a href="{{route('departamentos.index')}}"><i class="fas fa-building"></i>
            <h3 class="heading">Departamentos</h3>
            </a></article>
          <article class="one_half first"><a href="{{route('computadoras.index')}}"><i class="fas fa-desktop"></i>
            <h3 class="heading">Computadoras</h3>
            </a></article>
          <article class="one_half"><a href="{{route('impresoras.index')}}"><i class="fas fa-print"></i>
            <h3 class="heading">Impresoras</h3>
            </a></article>
          <!-- ################################################################################################ -->
        </div>
      </div>
    </div>
    <!-- ################################################################################################ -->
    <!-- / main body -->
    <div class="clear"></div>
  </main>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper bgded overlay">
  <section class="hoc container clear">
    <!-- ################################################################################################ -->
    <div class="center btmspace-80">
      <h2 class="font-x3">Lista de Pendientes de Mantenimiento en Departamentos</h2>
      <p>Mas recientes...</p>
    </div>
    <ul class="nospace group">
      <li class="one_half first post">
        <article class="group">
          <figure><img src="images/demo/320x320.png" alt="">
            <figcaption>
              <time datetime="2045-04-06T08:15+00:00"><strong>06</strong> <em>Apr</em></time>
              <div><i class="fas fa-comments"></i> <a href="#">5</a></div>
            </figcaption>
          </figure>
          <div class="txtwrap">
            <h6 class="heading">DEPARTAMENTO</h6>
            <p>Pendiente de arreglar... ;</p>
            <footer><a href="#">Read More</a></footer>
          </div>
        </article>
      </li>
      <li class="one_half post">
        <article class="group">
          <figure><img src="images/demo/320x320.png" alt="">
            <figcaption>
              <time datetime="2045-04-06T08:15+00:00"><strong>06</strong> <em>Apr</em></time>
              <div><i class="fas fa-comments"></i> <a href="#">5</a></div>
            </figcaption>
          </figure>
          <div class="txtwrap">
            <h6 class="heading">EQUIPO</h6>
            <p>Fallas del equipo...;</p>
            <footer><a href="#">Read More</a></footer>
          </div>
        </article>
      </li>
    </ul>
    <!-- ################################################################################################ -->
  </section>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row3">
  <div class="hoc container clear center">
    <!-- ################################################################################################ -->
    <i class="fas fa-4x fa-handshake btmspace-50"></i>

    <!-- ################################################################################################ -->
  </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row3 bgded overlay light" style="background-image:url('/images/demo/backgrounds/03.png');">
  <section id="team" class="clear">
    <!-- ################################################################################################ -->
    <div class="hoc container clear center">
      <h4 class="heading">ENCOMWEB</h4>
      <p>EQUIPO DE DESARROLLO ENE-JUN 2019</p>
      <p>Alonso Gutiérrez | José Raul Rivas | José Martin Burciaga | Jesús José López | Ernesto García | Ana Manriquez | Sandra Olivares | Jaqueline Quiroz | Miriam Janeth Rodríguez | Arely Lopez</p>
    </div>
    <!-- ################################################################################################ -->
  </section>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row3">
  <div id="testimonials" class="hoc container clear">
    <!-- ################################################################################################ -->
    <ul class="nospace group">
      <li class="one_half first">
        <p>MISION</p>
        <blockquote> Formar profesionistas de excelencia en el ámbito de la ciencia y la tecnología, capaces de propiciar el desarrollo y transformación de su entorno, a través de programas educativos de calidad pertinentes..</blockquote>

        </figure>
      </li>
      <li class="one_half">
        <p>VISION</p>
        <blockquote>Ser una institución educativa que se distinga como uno de los elementos fundamentales del desarrollo sustentable, permanente y equitativo del estado..</blockquote>

        </figure>
      </li>
    </ul>
    <!-- ################################################################################################ -->
  </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->

@endsection
