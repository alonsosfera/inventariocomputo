//Inicio de codigo JavaScript


  var $table = $('#tableInv')
  var $tableI = $('#tableI')
  var $tableC = $('#tableC')
  var $tableComp = $('#tableComp')

  function SelectC(){
    console.log('Entró');
  }
  function queryParams(params) {
    var url = window.location.pathname;
    var id = url.substring(url.lastIndexOf('/') + 1);
    params.id = id // add param1
    return params
  }

  $(function(){

    if($('#EditMessage').text().includes('Actualizado')){
      toastr.success('Actualizado correctamente', 'Exito!', {timeOut: 3000});
    }else if ($('#EditMessage').text().includes('Agregado')) {
      toastr.success('Creado correctamente', 'Exito!', {timeOut: 3000});
    }
  })

  $(function() {
    function GetSelected(){
      var ids = $.map($table.bootstrapTable('getSelections'), function (row) {
        return row.id
      })
      return ids;
    }

    $('#tableI').click(function(){
      var ids = $.map($tableI.bootstrapTable('getSelections'), function (row) {
        return row.id
      })
      $('#EditarI').attr('href', '/impresoras/'+ids+'/edit');
      $('#EliminarI').attr('href', ids);
    })

    $('#tableC').click(function(){
      var ids = $.map($tableC.bootstrapTable('getSelections'), function (row) {
        return row.id
      })
      $('#EditarC').attr('href', '/computadoras/'+ids+'/edit');
      $('#EliminarC').attr('href', ids);
    })

    $('#EliminarC').click(function(e){
      e.preventDefault();
      if($('#EliminarC').attr('href')==""){
        return;
      }
      if (window.confirm("¿Estas seguro?")) {
        $.post({
            type: 'DELETE',
            url: '/computadoras/'+$('#EliminarC').attr('href'),
            data: {  _token: $('meta[name="csrf-token"]').attr('content') },
        }).done(function (data) {
          $tableC.bootstrapTable('remove', {
            field: 'id',
            values: $('#EliminarC').attr('href')
          })
          toastr.success(data, 'Exito!', {timeOut: 3000});
        });
      }
    })

    $('#EliminarI').click(function(e){
      e.preventDefault();
      if($('#EliminarI').attr('href')==""){
        return;
      }
      if (window.confirm("¿Estas seguro que deseas eliminar?")) {
        $.post({
            type: 'DELETE',
            url: '/impresoras/'+$('#EliminarI').attr('href'),
            data: {  _token: $('meta[name="csrf-token"]').attr('content') },
        }).done(function (data) {
          $tableI.bootstrapTable('remove', {
            field: 'id',
            values: $('#EliminarI').attr('href')
          })
          toastr.success(data, 'Exito!', {timeOut: 3000});
        });
      }
    })

    $('#BtnEliminar').click(function () {
      var id = GetSelected();
      if(id== ""){
        return;
      }
      if (window.confirm("¿Estas seguro que deseas eliminar?")) {
        $table.bootstrapTable('remove', {
          field: 'id',
          values: id
        })
        $.ajax({
          type: 'DELETE',
          url: $('#ActiveRoute').text()+id,
          data: {  _token: $('meta[name="csrf-token"]').attr('content') },
          success: function(data){
            toastr.success(data, 'Exito!', {timeOut: 3000});
          }
        });
      }
    })

    $('#BtnEditar').click(function () {
      var id = GetSelected();
      if(id== ""){
        return;
      }
      $(location).attr("href", $('#ActiveRoute').text()+id+'/edit');
    })

    $('#BtnMostrar').click(function () {
      var id = GetSelected();
      if(id== ""){
        return;
      }
      $(location).attr("href", '/departamentos/'+id);
    })

    $('#BtnPsw').click(function () {
      var id = GetSelected();
      if(id== ""){
        return;
      }
      var usuario = $.map($table.bootstrapTable('getSelections'), function (row) {
        return row.username
      })
      if (window.confirm("¿Restablecer contraseña de "+usuario+"?")) {
        $.ajax({
          type: 'post',
          url: '/rusuarios/'+id,
          data: {  _token: $('meta[name="csrf-token"]').attr('content') },
          success: function(data){
            toastr.success(data, 'Exito!', {timeOut: 3000});
          }
        });
      }
    })



    $('#InventarioModal').on('shown.bs.modal', function () {
      $tableComp.bootstrapTable('resetView')
    })

  })
