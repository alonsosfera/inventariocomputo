<?php

namespace App\Http\Controllers;

use App\Mantenimiento;
use Illuminate\Http\Request;

class MantenimientoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('mantenimientos.index');
    }

    public function table()
    {
        $mantenimientos = Mantenimiento::all();
        return $mantenimientos;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $mantenimientos = Mantenimiento::all();
        return view('mantenimientos.create', compact('mantenimientos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'id_Equipo' => 'required',
            'tipo_equipo'=> 'required | integer',
            'usuario'=> 'required',
            'acciones'=> 'required | integer',
            'observaciones'=> 'required'

        ]);

        $mantenimiento = new Mantenimiento([
            'id_Equipo'=>$request->get('id_Equipo'),
            'tipo_equipo'=> $request->get('tipo_equipo'),
            'usuario'=> $request->get('usuario'),
            'acciones'=> $request->get('acciones'),
            'observaciones'=> $request->get('observaciones')


        ]);
        $mantenimiento->save();
        return redirect('mantenimientos')->with('mensaje', 'Agregado con exito' );

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Mantenimiento  $mantenimiento
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return Mantenimiento::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Mantenimiento  $mantenimiento
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $mantenimiento = Mantenimiento::findOrFail($id);
        return view('mantenimientos.edit', compact('mantenimiento'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mantenimiento  $mantenimiento
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Mantenimiento $mantenimiento)
    {
        $request->validate([
            'id_Equipo' => 'required',
            'tipo_equipo'=> 'required | integer',
            'usuario'=> 'required',
            'acciones'=> 'required | integer',
            'observaciones'=> 'required'

        ]);

        $mantenimiento = Mantenimiento::find($id);
        $mantenimiento->id_Equipo= $request->get('id_Equipo');
        $mantenimiento->tipo_equipo = $request->get('tipo_equipo');
        $mantenimiento->usuario= $request->get('usuario');
        $mantenimiento->acciones= $request->get('observaciones');


       $mantenimiento->save();

        return redirect('mantenimientos')->with('mensaje', 'Actualizado con exito' );

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Mantenimiento  $mantenimiento
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $mantenimiento = Mantenimiento::find($id);
        $mantenimiento->delete();

        return "Eliminado correctamente";
    }
}
