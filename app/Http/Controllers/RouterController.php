<?php

namespace App\Http\Controllers;

use App\Router;
use Illuminate\Http\Request;

class RouterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
   {
       $this->middleware('auth');
   }

   public function new()
   {
       return view('routers.new');
   }
    public function index()
    {
        //
        return view('routers.index');
    }

    public function table()
    {
        $routers = Router::all();
        return $routers;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('routers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         $request->validate([
            'Marca'=> 'required',
            'Caracteristicas'=> 'required',
            'ip_wan'=> 'required',
            'ip_lan'=> 'required'
        ]);


        $router = new Router([
            'Marca'=> $request->get('Marca'),
            'Caracteristicas'=> $request->get('Caracteristicas'),
            'ip_lan'=> $request->get('ip_wan'),
            'ip_wan'=> $request->get('ip_wan')

        ]);
        $router->save();
        return redirect('routers')->with('mensaje', 'Agregado con exito' );


    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Router  $router
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return Router::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Router  $router
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $router = Router::findOrFail($id);
        return view('routers.edit', compact('router'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Router  $router
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'Marca'=> 'required',
            'ip_lan'=> 'required',
            'ip_wan'=> 'required',
        ]);

        $router = Router::find($id);
        $router->Marca= $request->get('Marca');
        $router->Caracteristicas= $request->get('Caracteristicas');
        $router->ip_lan= $request->get('ip_lan');
        $router->ip_wan= $request->get('ip_wan');

       $router->save();

        return redirect('routers')->with('mensaje', 'Actualizado con exito' );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Router  $router
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $router = Router::find($id);
        $router->delete();
        return "Eliminado correctamente";

    }

}
