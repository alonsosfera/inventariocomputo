<?php

namespace App\Http\Controllers;

use App\Departamento;
use App\Computadora;
use App\SwitchM;
use App\Router;
use App\Impresora;
use DB;
use Illuminate\Http\Request;

class DepartamentosController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    //metodo de listar datos
    return view('departamentos.index');

  }

  public function table()
  {
      $departamentos = Departamento::all();
      return $departamentos;
  }


   /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('departamentos.create');

        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([

            'nombre'=> 'required'
        ]);


        $departamento = new Departamento([

                       'nombre'=> $request->get('nombre')
        ]);
        $departamento->save();
        return redirect('departamentos')->with('mensaje', 'Agregado con exito' );


            }

    /**
     * Display the specified resource.
     *
     * @param  \App\Computadora  $computadora
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $computadoras = Computadora::where('Departamento', '=', $id)->get();
      $impresoras = Impresora::where('Departamento', '=', $id)->get();
      $departamento = Departamento::findOrFail($id);


      return view('departamentos.show', compact('computadoras', 'impresoras', 'departamento'));

        /*$departamentos = Departamento::all();



        foreach ($departamentos as $departamento) {

            $deps[$departamento['id']]=$departamento['nombre'];
        }

      $sql = "SELECT * FROM computadoras WHERE Departamento=? ";
        $computadoras= DB::select($sql,array($id));
         return view('departamentos.show', compact('computadoras', 'departamentos','deps'));*/
     }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Computadora  $computadora
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //editar computadpra
        $departamento = Departamento::findOrFail($id);
        return view('departamentos.edit', compact('departamento'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Computadora  $computadora
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
      $request->validate([

            'nombre'=> 'required'
        ]);


        $departamento = Departamento::find($id);
        $departamento->id = $request->get('id');
        $departamento->nombre= $request->get('nombre');

       $departamento->save();

        return redirect('departamentos')->with('mensaje', 'Actualizado con exito' );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Computadora  $computadora
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $departamento = Departamento::find($id);
        $departamento->delete();

        return redirect('departamentos')->with('mensaje','Registro Eliminado');

    }
}
