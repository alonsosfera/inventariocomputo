<?php

namespace App\Http\Controllers;

use App\SwitchM;
use Illuminate\Http\Request;

class SwitchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

   public function __construct()
   {
       $this->middleware('auth');
   }

   public function new()
   {
       return view('switches.new');
   }

    public function index()
    {
        return view('switches.index');
    }

    public function table()
    {
        $Switches = SwitchM::all();
        return $Switches;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('switches.create');

        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'Marca' => 'required',
            'Caracteristicas'=> 'required',
            'ip_admin'=> 'required',
        ]);

        $Switch = new SwitchM([
            'Marca'=> $request->get('Marca'),
            'Caracteristicas'=> $request->get('Caracteristicas'),
            'ip_admin'=>$request->get('ip_admin')

        ]);
        $Switch->save();
        return redirect('switches')->with('mensaje', 'Agregado con exito' );
      }


        //
        /**
         * Display the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            return SwitchM::find($id);
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            //
            $switch = SwitchM::findOrFail($id);
            return view('switches.edit', compact('switch'));
        }

        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            //
            $request->validate([
                'Marca' => 'required',
                'Caracteristicas'=> 'required',
                'ip_admin'=> 'required',
            ]);

            $Switch = SwitchM::find($id);
            $Switch->Marca= $request->get('Marca');
            $Switch->Caracteristicas = $request->get('Caracteristicas');
            $Switch->ip_admin= $request->get('ip_admin');
            $Switch->save();

            return redirect('switches')->with('mensaje', 'Actualizado con exito' );
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
         // public function destroy(Computadora $computadora)
       public function destroy($id)
        {
            //
            $Switch = SwitchM::find($id);
            $Switch->delete();
            return "Eliminado correctamente";

        }

 }
