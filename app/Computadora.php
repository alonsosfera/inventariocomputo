<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Computadora extends Model
{
    //modelo de la tabla computadoras de la base de datos invetec

    protected $fillable=[
    	'id',
      'Nombre',
    	'SO',
    	'Ram',
    	'Disco',
    	'Procesador',
    	'Modelo',
    	'Departamento',
    	'Caracteristicas',
    	'encargado',
      'Direccion_Ip'

    ];

    public function departamento() {
        return $this->hasOne('App\Departamento', 'id', 'Departamento');
    }
}
