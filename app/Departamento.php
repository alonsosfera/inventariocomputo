<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Departamento extends Model
{
    //

    protected $fillable =[
    	'id',
    	'nombre'
    ];

    public function depComp() {
        return $this->hasMany('App\Computadora', 'id', 'Departamento');
    }

    public function deptImp() {
        return $this->hasMany('App\Impresora', 'id', 'Departamento');
    }

    public function deptRou() {
        return $this->hasMany('App\Router', 'id', 'Departamento');
    }

    public function deptSwi() {
        return $this->hasMany('App\SwitchM', 'id', 'Departamento');
    }

}
