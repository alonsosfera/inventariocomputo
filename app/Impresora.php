<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Impresora extends Model
{
    //

    protected $fillable=[
    	'id',
    	'Marca',
    	'Caracteristicas',
    	'Departamento'
    ];

    public function departamento() {
        return $this->hasOne('App\Departamento', 'id', 'Departamento');
    }
}
