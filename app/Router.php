<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Router extends Model
{
    //
    protected $table = "router";
    //public $timestamps = false;
     protected $fillable=[
    	'id',
    	'Marca',
    	'Caracteristicas',
    	'ip_lan',
    	'ip_wan'
    ];
}
